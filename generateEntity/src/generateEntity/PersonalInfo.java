package generateEntity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PERSONAL_INFO database table.
 * 
 */
@Entity
@Table(name="PERSONAL_INFO")
@NamedQuery(name="PersonalInfo.findAll", query="SELECT p FROM PersonalInfo p")
public class PersonalInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="PERSON_ID", unique=true, nullable=false)
	private long personId;

	@Column(name="BLOOD_GROUP", length=20)
	private String bloodGroup;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date dob;

	@Column(name="EMAIL_ID", length=100)
	private String emailId;

	@Column(name="F_NAME", length=100)
	private String fName;

	@Column(nullable=false, length=1)
	private String gender;

	@Column(name="L_NAME", length=100)
	private String lName;

	@Column(name="M_NAME", length=100)
	private String mName;

	@Column(name="MOBILE_NUMBER", length=20)
	private String mobileNumber;

	@Column(name="WHATSAPP_NUMBER", length=20)
	private String whatsappNumber;

	//bi-directional many-to-one association to GaurdianMaster
	@OneToMany(mappedBy="personalInfo")
	private List<GaurdianMaster> gaurdianMasters;

	//bi-directional many-to-one association to AddressMaster
	@ManyToOne
	@JoinColumn(name="ADDRESS_ID")
	private AddressMaster addressMaster;

	//bi-directional many-to-one association to StaffMaster
	@OneToMany(mappedBy="personalInfo")
	private List<StaffMaster> staffMasters;

	//bi-directional many-to-one association to StudentMaster
	@OneToMany(mappedBy="personalInfo")
	private List<StudentMaster> studentMasters;

	public PersonalInfo() {
	}

	public long getPersonId() {
		return this.personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public String getBloodGroup() {
		return this.bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getEmailId() {
		return this.emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFName() {
		return this.fName;
	}

	public void setFName(String fName) {
		this.fName = fName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLName() {
		return this.lName;
	}

	public void setLName(String lName) {
		this.lName = lName;
	}

	public String getMName() {
		return this.mName;
	}

	public void setMName(String mName) {
		this.mName = mName;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getWhatsappNumber() {
		return this.whatsappNumber;
	}

	public void setWhatsappNumber(String whatsappNumber) {
		this.whatsappNumber = whatsappNumber;
	}

	public List<GaurdianMaster> getGaurdianMasters() {
		return this.gaurdianMasters;
	}

	public void setGaurdianMasters(List<GaurdianMaster> gaurdianMasters) {
		this.gaurdianMasters = gaurdianMasters;
	}

	public GaurdianMaster addGaurdianMaster(GaurdianMaster gaurdianMaster) {
		getGaurdianMasters().add(gaurdianMaster);
		gaurdianMaster.setPersonalInfo(this);

		return gaurdianMaster;
	}

	public GaurdianMaster removeGaurdianMaster(GaurdianMaster gaurdianMaster) {
		getGaurdianMasters().remove(gaurdianMaster);
		gaurdianMaster.setPersonalInfo(null);

		return gaurdianMaster;
	}

	public AddressMaster getAddressMaster() {
		return this.addressMaster;
	}

	public void setAddressMaster(AddressMaster addressMaster) {
		this.addressMaster = addressMaster;
	}

	public List<StaffMaster> getStaffMasters() {
		return this.staffMasters;
	}

	public void setStaffMasters(List<StaffMaster> staffMasters) {
		this.staffMasters = staffMasters;
	}

	public StaffMaster addStaffMaster(StaffMaster staffMaster) {
		getStaffMasters().add(staffMaster);
		staffMaster.setPersonalInfo(this);

		return staffMaster;
	}

	public StaffMaster removeStaffMaster(StaffMaster staffMaster) {
		getStaffMasters().remove(staffMaster);
		staffMaster.setPersonalInfo(null);

		return staffMaster;
	}

	public List<StudentMaster> getStudentMasters() {
		return this.studentMasters;
	}

	public void setStudentMasters(List<StudentMaster> studentMasters) {
		this.studentMasters = studentMasters;
	}

	public StudentMaster addStudentMaster(StudentMaster studentMaster) {
		getStudentMasters().add(studentMaster);
		studentMaster.setPersonalInfo(this);

		return studentMaster;
	}

	public StudentMaster removeStudentMaster(StudentMaster studentMaster) {
		getStudentMasters().remove(studentMaster);
		studentMaster.setPersonalInfo(null);

		return studentMaster;
	}

}