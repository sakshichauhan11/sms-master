package generateEntity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the SCHOOL_GROUP_MASTER database table.
 * 
 */
@Entity
@Table(name="SCHOOL_GROUP_MASTER")
@NamedQuery(name="SchoolGroupMaster.findAll", query="SELECT s FROM SchoolGroupMaster s")
public class SchoolGroupMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="GROUP_ID", unique=true, nullable=false)
	private long groupId;

	@Column(name="GROUP_NAME", length=100)
	private String groupName;

	//bi-directional many-to-one association to SchoolMaster
	@OneToMany(mappedBy="schoolGroupMaster")
	private List<SchoolMaster> schoolMasters;

	public SchoolGroupMaster() {
	}

	public long getGroupId() {
		return this.groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return this.groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<SchoolMaster> getSchoolMasters() {
		return this.schoolMasters;
	}

	public void setSchoolMasters(List<SchoolMaster> schoolMasters) {
		this.schoolMasters = schoolMasters;
	}

	public SchoolMaster addSchoolMaster(SchoolMaster schoolMaster) {
		getSchoolMasters().add(schoolMaster);
		schoolMaster.setSchoolGroupMaster(this);

		return schoolMaster;
	}

	public SchoolMaster removeSchoolMaster(SchoolMaster schoolMaster) {
		getSchoolMasters().remove(schoolMaster);
		schoolMaster.setSchoolGroupMaster(null);

		return schoolMaster;
	}

}