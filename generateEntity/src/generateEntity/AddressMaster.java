package generateEntity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the ADDRESS_MASTER database table.
 * 
 */
@Entity
@Table(name="ADDRESS_MASTER")
@NamedQuery(name="AddressMaster.findAll", query="SELECT a FROM AddressMaster a")
public class AddressMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ADDRESS_ID", unique=true, nullable=false)
	private long addressId;

	@Column(name="ADDRESS_LINE_1", nullable=false, length=100)
	private String addressLine1;

	@Column(name="ADDRESS_LINE_2", length=100)
	private String addressLine2;

	//bi-directional many-to-one association to PincodeMaster
	@ManyToOne
	@JoinColumn(name="PINCODE")
	private PincodeMaster pincodeMaster;

	//bi-directional many-to-one association to PersonalInfo
	@OneToMany(mappedBy="addressMaster")
	private List<PersonalInfo> personalInfos;

	//bi-directional many-to-one association to SchoolMaster
	@OneToMany(mappedBy="addressMaster")
	private List<SchoolMaster> schoolMasters;

	public AddressMaster() {
	}

	public long getAddressId() {
		return this.addressId;
	}

	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}

	public String getAddressLine1() {
		return this.addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return this.addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public PincodeMaster getPincodeMaster() {
		return this.pincodeMaster;
	}

	public void setPincodeMaster(PincodeMaster pincodeMaster) {
		this.pincodeMaster = pincodeMaster;
	}

	public List<PersonalInfo> getPersonalInfos() {
		return this.personalInfos;
	}

	public void setPersonalInfos(List<PersonalInfo> personalInfos) {
		this.personalInfos = personalInfos;
	}

	public PersonalInfo addPersonalInfo(PersonalInfo personalInfo) {
		getPersonalInfos().add(personalInfo);
		personalInfo.setAddressMaster(this);

		return personalInfo;
	}

	public PersonalInfo removePersonalInfo(PersonalInfo personalInfo) {
		getPersonalInfos().remove(personalInfo);
		personalInfo.setAddressMaster(null);

		return personalInfo;
	}

	public List<SchoolMaster> getSchoolMasters() {
		return this.schoolMasters;
	}

	public void setSchoolMasters(List<SchoolMaster> schoolMasters) {
		this.schoolMasters = schoolMasters;
	}

	public SchoolMaster addSchoolMaster(SchoolMaster schoolMaster) {
		getSchoolMasters().add(schoolMaster);
		schoolMaster.setAddressMaster(this);

		return schoolMaster;
	}

	public SchoolMaster removeSchoolMaster(SchoolMaster schoolMaster) {
		getSchoolMasters().remove(schoolMaster);
		schoolMaster.setAddressMaster(null);

		return schoolMaster;
	}

}