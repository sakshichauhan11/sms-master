package generateEntity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the SCHOOL_MASTER database table.
 * 
 */
@Entity
@Table(name="SCHOOL_MASTER")
@NamedQuery(name="SchoolMaster.findAll", query="SELECT s FROM SchoolMaster s")
public class SchoolMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="SCHOOL_ID", unique=true, nullable=false)
	private long schoolId;

	@Column(length=100)
	private String name;

	@Column(name="PRINCIPAL_ID")
	private BigDecimal principalId;

	//bi-directional many-to-one association to MarksRecord
	@OneToMany(mappedBy="schoolMaster")
	private List<MarksRecord> marksRecords;

	//bi-directional many-to-one association to AddressMaster
	@ManyToOne
	@JoinColumn(name="ADDRESS_ID")
	private AddressMaster addressMaster;

	//bi-directional many-to-one association to SchoolGroupMaster
	@ManyToOne
	@JoinColumn(name="GROUP_ID")
	private SchoolGroupMaster schoolGroupMaster;

	public SchoolMaster() {
	}

	public long getSchoolId() {
		return this.schoolId;
	}

	public void setSchoolId(long schoolId) {
		this.schoolId = schoolId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrincipalId() {
		return this.principalId;
	}

	public void setPrincipalId(BigDecimal principalId) {
		this.principalId = principalId;
	}

	public List<MarksRecord> getMarksRecords() {
		return this.marksRecords;
	}

	public void setMarksRecords(List<MarksRecord> marksRecords) {
		this.marksRecords = marksRecords;
	}

	public MarksRecord addMarksRecord(MarksRecord marksRecord) {
		getMarksRecords().add(marksRecord);
		marksRecord.setSchoolMaster(this);

		return marksRecord;
	}

	public MarksRecord removeMarksRecord(MarksRecord marksRecord) {
		getMarksRecords().remove(marksRecord);
		marksRecord.setSchoolMaster(null);

		return marksRecord;
	}

	public AddressMaster getAddressMaster() {
		return this.addressMaster;
	}

	public void setAddressMaster(AddressMaster addressMaster) {
		this.addressMaster = addressMaster;
	}

	public SchoolGroupMaster getSchoolGroupMaster() {
		return this.schoolGroupMaster;
	}

	public void setSchoolGroupMaster(SchoolGroupMaster schoolGroupMaster) {
		this.schoolGroupMaster = schoolGroupMaster;
	}

}