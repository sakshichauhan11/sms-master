package generateEntity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the PINCODE_MASTER database table.
 * 
 */
@Entity
@Table(name="PINCODE_MASTER")
@NamedQuery(name="PincodeMaster.findAll", query="SELECT p FROM PincodeMaster p")
public class PincodeMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private long pincode;

	@Column(length=20)
	private String area;

	//bi-directional many-to-one association to AddressMaster
	@OneToMany(mappedBy="pincodeMaster")
	private List<AddressMaster> addressMasters;

	//bi-directional many-to-one association to CityMaster
	@ManyToOne
	@JoinColumn(name="CITY_ID")
	private CityMaster cityMaster;

	public PincodeMaster() {
	}

	public long getPincode() {
		return this.pincode;
	}

	public void setPincode(long pincode) {
		this.pincode = pincode;
	}

	public String getArea() {
		return this.area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public List<AddressMaster> getAddressMasters() {
		return this.addressMasters;
	}

	public void setAddressMasters(List<AddressMaster> addressMasters) {
		this.addressMasters = addressMasters;
	}

	public AddressMaster addAddressMaster(AddressMaster addressMaster) {
		getAddressMasters().add(addressMaster);
		addressMaster.setPincodeMaster(this);

		return addressMaster;
	}

	public AddressMaster removeAddressMaster(AddressMaster addressMaster) {
		getAddressMasters().remove(addressMaster);
		addressMaster.setPincodeMaster(null);

		return addressMaster;
	}

	public CityMaster getCityMaster() {
		return this.cityMaster;
	}

	public void setCityMaster(CityMaster cityMaster) {
		this.cityMaster = cityMaster;
	}

}