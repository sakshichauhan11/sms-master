package generateEntity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the EXAM_MASTER database table.
 * 
 */
@Entity
@Table(name="EXAM_MASTER")
@NamedQuery(name="ExamMaster.findAll", query="SELECT e FROM ExamMaster e")
public class ExamMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="EXAM_ID", unique=true, nullable=false)
	private long examId;

	@Column(name="EXAM_NAME", length=50)
	private String examName;

	//bi-directional many-to-one association to ExamType
	@ManyToOne
	@JoinColumn(name="EXAM_TYPE_ID")
	private ExamType examType;

	//bi-directional many-to-one association to MarksRecord
	@OneToMany(mappedBy="examMaster")
	private List<MarksRecord> marksRecords;

	public ExamMaster() {
	}

	public long getExamId() {
		return this.examId;
	}

	public void setExamId(long examId) {
		this.examId = examId;
	}

	public String getExamName() {
		return this.examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public ExamType getExamType() {
		return this.examType;
	}

	public void setExamType(ExamType examType) {
		this.examType = examType;
	}

	public List<MarksRecord> getMarksRecords() {
		return this.marksRecords;
	}

	public void setMarksRecords(List<MarksRecord> marksRecords) {
		this.marksRecords = marksRecords;
	}

	public MarksRecord addMarksRecord(MarksRecord marksRecord) {
		getMarksRecords().add(marksRecord);
		marksRecord.setExamMaster(this);

		return marksRecord;
	}

	public MarksRecord removeMarksRecord(MarksRecord marksRecord) {
		getMarksRecords().remove(marksRecord);
		marksRecord.setExamMaster(null);

		return marksRecord;
	}

}