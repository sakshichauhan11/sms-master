package generateEntity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the MARKS_RECORD database table.
 * 
 */
@Entity
@Table(name="MARKS_RECORD")
@NamedQuery(name="MarksRecord.findAll", query="SELECT m FROM MarksRecord m")
public class MarksRecord implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false, length=20)
	private String id;

	private BigDecimal marks;

	//bi-directional many-to-one association to ClassMaster
	@ManyToOne
	@JoinColumn(name="CLASS_ID")
	private ClassMaster classMaster;

	//bi-directional many-to-one association to ExamMaster
	@ManyToOne
	@JoinColumn(name="EXAM_ID")
	private ExamMaster examMaster;

	//bi-directional many-to-one association to SchoolMaster
	@ManyToOne
	@JoinColumn(name="SCHOOL_ID")
	private SchoolMaster schoolMaster;

	//bi-directional many-to-one association to StudentMaster
	@ManyToOne
	@JoinColumn(name="STUDENT_ID")
	private StudentMaster studentMaster;

	//bi-directional many-to-one association to SubjectMaster
	@ManyToOne
	@JoinColumn(name="SUBJECT_ID")
	private SubjectMaster subjectMaster;

	public MarksRecord() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getMarks() {
		return this.marks;
	}

	public void setMarks(BigDecimal marks) {
		this.marks = marks;
	}

	public ClassMaster getClassMaster() {
		return this.classMaster;
	}

	public void setClassMaster(ClassMaster classMaster) {
		this.classMaster = classMaster;
	}

	public ExamMaster getExamMaster() {
		return this.examMaster;
	}

	public void setExamMaster(ExamMaster examMaster) {
		this.examMaster = examMaster;
	}

	public SchoolMaster getSchoolMaster() {
		return this.schoolMaster;
	}

	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}

	public StudentMaster getStudentMaster() {
		return this.studentMaster;
	}

	public void setStudentMaster(StudentMaster studentMaster) {
		this.studentMaster = studentMaster;
	}

	public SubjectMaster getSubjectMaster() {
		return this.subjectMaster;
	}

	public void setSubjectMaster(SubjectMaster subjectMaster) {
		this.subjectMaster = subjectMaster;
	}

}