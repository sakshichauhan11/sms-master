package generateEntity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the SUBJECT_MASTER database table.
 * 
 */
@Entity
@Table(name="SUBJECT_MASTER")
@NamedQuery(name="SubjectMaster.findAll", query="SELECT s FROM SubjectMaster s")
public class SubjectMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="SUBJECT_ID", unique=true, nullable=false)
	private long subjectId;

	@Column(length=20)
	private String code;

	@Column(length=20)
	private String name;

	//bi-directional many-to-one association to MarksRecord
	@OneToMany(mappedBy="subjectMaster")
	private List<MarksRecord> marksRecords;

	public SubjectMaster() {
	}

	public long getSubjectId() {
		return this.subjectId;
	}

	public void setSubjectId(long subjectId) {
		this.subjectId = subjectId;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<MarksRecord> getMarksRecords() {
		return this.marksRecords;
	}

	public void setMarksRecords(List<MarksRecord> marksRecords) {
		this.marksRecords = marksRecords;
	}

	public MarksRecord addMarksRecord(MarksRecord marksRecord) {
		getMarksRecords().add(marksRecord);
		marksRecord.setSubjectMaster(this);

		return marksRecord;
	}

	public MarksRecord removeMarksRecord(MarksRecord marksRecord) {
		getMarksRecords().remove(marksRecord);
		marksRecord.setSubjectMaster(null);

		return marksRecord;
	}

}