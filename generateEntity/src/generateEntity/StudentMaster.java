package generateEntity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the STUDENT_MASTER database table.
 * 
 */
@Entity
@Table(name="STUDENT_MASTER")
@NamedQuery(name="StudentMaster.findAll", query="SELECT s FROM StudentMaster s")
public class StudentMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="STUDENT_ID", unique=true, nullable=false)
	private long studentId;

	@Column(name="UNIQUE_ID", nullable=false)
	private BigDecimal uniqueId;

	//bi-directional many-to-one association to MarksRecord
	@OneToMany(mappedBy="studentMaster")
	private List<MarksRecord> marksRecords;

	//bi-directional many-to-one association to GaurdianMaster
	@ManyToOne
	@JoinColumn(name="GAURDIAN_ID")
	private GaurdianMaster gaurdianMaster;

	//bi-directional many-to-one association to PersonalInfo
	@ManyToOne
	@JoinColumn(name="PERSON_ID")
	private PersonalInfo personalInfo;

	public StudentMaster() {
	}

	public long getStudentId() {
		return this.studentId;
	}

	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}

	public BigDecimal getUniqueId() {
		return this.uniqueId;
	}

	public void setUniqueId(BigDecimal uniqueId) {
		this.uniqueId = uniqueId;
	}

	public List<MarksRecord> getMarksRecords() {
		return this.marksRecords;
	}

	public void setMarksRecords(List<MarksRecord> marksRecords) {
		this.marksRecords = marksRecords;
	}

	public MarksRecord addMarksRecord(MarksRecord marksRecord) {
		getMarksRecords().add(marksRecord);
		marksRecord.setStudentMaster(this);

		return marksRecord;
	}

	public MarksRecord removeMarksRecord(MarksRecord marksRecord) {
		getMarksRecords().remove(marksRecord);
		marksRecord.setStudentMaster(null);

		return marksRecord;
	}

	public GaurdianMaster getGaurdianMaster() {
		return this.gaurdianMaster;
	}

	public void setGaurdianMaster(GaurdianMaster gaurdianMaster) {
		this.gaurdianMaster = gaurdianMaster;
	}

	public PersonalInfo getPersonalInfo() {
		return this.personalInfo;
	}

	public void setPersonalInfo(PersonalInfo personalInfo) {
		this.personalInfo = personalInfo;
	}

}