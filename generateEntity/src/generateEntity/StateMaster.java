package generateEntity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the STATE_MASTER database table.
 * 
 */
@Entity
@Table(name="STATE_MASTER")
@NamedQuery(name="StateMaster.findAll", query="SELECT s FROM StateMaster s")
public class StateMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="STATE_ID", unique=true, nullable=false)
	private long stateId;

	@Column(length=100)
	private String name;

	//bi-directional many-to-one association to CityMaster
	@OneToMany(mappedBy="stateMaster")
	private List<CityMaster> cityMasters;

	//bi-directional many-to-one association to CountryMaster
	@ManyToOne
	@JoinColumn(name="COUNTRY_ID")
	private CountryMaster countryMaster;

	public StateMaster() {
	}

	public long getStateId() {
		return this.stateId;
	}

	public void setStateId(long stateId) {
		this.stateId = stateId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CityMaster> getCityMasters() {
		return this.cityMasters;
	}

	public void setCityMasters(List<CityMaster> cityMasters) {
		this.cityMasters = cityMasters;
	}

	public CityMaster addCityMaster(CityMaster cityMaster) {
		getCityMasters().add(cityMaster);
		cityMaster.setStateMaster(this);

		return cityMaster;
	}

	public CityMaster removeCityMaster(CityMaster cityMaster) {
		getCityMasters().remove(cityMaster);
		cityMaster.setStateMaster(null);

		return cityMaster;
	}

	public CountryMaster getCountryMaster() {
		return this.countryMaster;
	}

	public void setCountryMaster(CountryMaster countryMaster) {
		this.countryMaster = countryMaster;
	}

}